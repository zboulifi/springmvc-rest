package tn.esprit.spring.controller;

import tn.esprit.spring.entity.User;

import java.util.List;

public interface IUserController {
	 void addUser(User user);
	 String getFirstNameByUserId(Long userId);
	 String getLastNameByUserId(Long userId);
	 void deleteUser(Long userId) ;
	 List<User> getListUserByLastName(String lastName);
	 List<User> getListUserByFirstName(String firstName);

}
