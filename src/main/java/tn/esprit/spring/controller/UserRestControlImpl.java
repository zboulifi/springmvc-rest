package tn.esprit.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.entity.User;
import tn.esprit.spring.service.interfaces.IUserService;

import java.util.List;

@RestController
public class UserRestControlImpl {
    @Autowired
    IUserService userService;

    // http://localhost:8081/SpringMVC/servlet/
    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Web Services REST Spring. "; }

    // URL : http://localhost:8081/SpringMVC/servlet/retrieve-all-users
       @GetMapping("/retrieve-all-users")
        @ResponseBody
        public List<User> getUsers () {
            List<User> list = userService.retrieveAllUsers();
            return list;
        }
        // http://localhost:8081/SpringMVC/servlet/ retrieve-user /{user-id}
        @GetMapping("/retrieve-user/{user-id}")
        @ResponseBody
        public User getEmployee (@PathVariable("user-id") Long userId){
            return userService.retrieveUser(userId);
        }

    // Ajouter User : http://localhost:8081/SpringMVC/servlet/add-user
    @PostMapping("/add-user")
    @ResponseBody
    public User addUser(@RequestBody User u) {

         userService.addUser(u);

        return u;
    }
    // Supprimer User : http://localhost:8081/SpringMVC/servlet/ delete-user/{user-id}
    @DeleteMapping("/delete-user/{user-id}")
    @ResponseBody
    public void deleteEmployee(@PathVariable("user-id") Long userId) {
        userService.deleteUser(userId);
    }
    // Modifier User
    // http://localhost:8081/SpringMVC/servlet/modify-user
    @PutMapping("/modify-user")
    @ResponseBody
    public User updateEmployee(@RequestBody User user) {
        return userService.updateUser(user);
    }
    }
