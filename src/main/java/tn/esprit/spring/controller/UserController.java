package tn.esprit.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import tn.esprit.spring.entity.User;
import tn.esprit.spring.service.interfaces.IUserService;

import java.util.List;

@Controller
public class UserController implements IUserController{
	@Autowired
    IUserService userService;

	@Override
	public void addUser(User user) {
	   userService.addUser(user);	
	}

	@Override
	public String getFirstNameByUserId(Long userId) {
		return userService.getFirstNameByUserId(userId);
	}

	@Override
	public String getLastNameByUserId(Long userId) {
		return userService.getLastNameByUserId(userId);
	}

	@Override
	public void deleteUser(Long userId) {
		userService.deleteUser(userId);
	}

	@Override
	public List<User> getListUserByLastName(String lastName) {
		return userService.getListUserByLastName(lastName);
	}

	@Override
	public List<User> getListUserByFirstName(String firstName) {
		return userService.getListUserByFirstName(firstName);
	}
	
}
