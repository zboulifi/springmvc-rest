package tn.esprit.spring.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.spring.entity.User;

import java.util.List;

@Repository
public interface IUserRepository extends CrudRepository<User,Long> {
    List<User>findByLastName(String lastName);
    List<User>findByFirstName(String firstName);
    @Query("select Count(u) from User u")
    Long getMaxId();


}
