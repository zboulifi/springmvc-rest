package tn.esprit.spring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tn.esprit.spring.entity.User;
import tn.esprit.spring.repository.IUserRepository;
import tn.esprit.spring.service.interfaces.IUserService;

import java.util.List;

@Component
public class UserServiceImpl implements IUserService {

    @Autowired
    IUserRepository useReposotory;

    public void addUser(User user) {
        useReposotory.save(user);
    }
    public String getFirstNameByUserId(Long userId) {
        return useReposotory.findById(userId).get().getFirstName();
    }
    public String getLastNameByUserId(Long userId) {
        return useReposotory.findById(userId).get().getLastName();
    }

    @Override
    public void deleteUser(Long userId) {
        useReposotory.deleteById(userId);
    }

    public List<User> getListUserByLastName(String lastName){
        return useReposotory.findByLastName(lastName);
    }
    public List<User> getListUserByFirstName(String firstName){
        return useReposotory.findByFirstName(firstName);
    }

    @Override
    public List<User> retrieveAllUsers() {
        return (List<User>) useReposotory.findAll();
    }

    @Override
    public User retrieveUser(Long userId) {
        return useReposotory.findById(userId).get();
    }

    @Override
    public User updateUser(User user) {
        if(null!=user.getId()){
            return useReposotory.save(user);
        }
        return null;
    }
}
