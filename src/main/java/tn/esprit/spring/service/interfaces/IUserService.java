package tn.esprit.spring.service.interfaces;


import tn.esprit.spring.entity.User;

import java.util.List;

public interface IUserService  {
    void addUser(User user);
    String getFirstNameByUserId(Long userId);
    String getLastNameByUserId(Long userId);
    void deleteUser(Long userId) ;
    List<User> getListUserByLastName(String lastName);
    List<User> getListUserByFirstName(String firstName);
    List<User> retrieveAllUsers();
    User retrieveUser(Long userId);
    User updateUser(User user);
}
